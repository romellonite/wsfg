# General Imports
import sys, json
# Import Services
from services import Items, Questions, Promotion, UserProfile
# Import Bottle
from bottle import route, run, error, get, post, delete

# http://localhost:8080/wsfg/add-item/0

# Holds the Mock Data path
MOCK_DATA_PATH = '../../../../../../resources/mock-data/wsfg-mock-data.json'
data = {}

with open(MOCK_DATA_PATH) as f:
    data = json.load(f)

                    # G E T S
# Get All Items
@get('/wsfg/items')
def getAllItems():
    return json.dumps(Items.getAllItems(data))

# Get All Frequently Asked Questions
@get('/wsfg/faq')
def getAllFaq():
    return json.dumps(Questions.getAllFaq(data))

# Get Trending Items
@get('/wsfg/trending-items')
def getTrendingItems():
    return json.dumps(Items.getTrendingItems(data))

                    # P O S T S
# Send Question
@route('/wsfg/send-question/<questionForm>', method=['GET','POST'])
def sendQuestion(questionForm):
    print(questionForm)
    Questions.sendQuestion(questionForm)
    return

# Add Item
@post('/wsfg/add-item-to-cart/<itemId>')
def addItem(itemId):
    Items.addItem(itemId)

# Preform Login
@post('/wsfg/preform-login/<userCredentials>')
def preform_login(userCredentials):
    UserProfile.preform_login(userCredentials)

# Apply Promotion Code
@post('/wsfg/apply-promotion-code/<promotionCode>')
def applyPromotionCode(promotionCode):
    Promotion.applyPromotionCode(promotionCode)

                # D E L E T E S
# Add Item
@delete('/wsfg/delete-item-from-cart/<itemId>')
def deleteItem(itemId):
    Items.addItem(itemId)

                    # E R R O R S
# Error Message
@error(404)
def error404(error):
    return 'Nothing here, sorry :('

run(reLoader=True, host='localhost', port=8080, debug=True)