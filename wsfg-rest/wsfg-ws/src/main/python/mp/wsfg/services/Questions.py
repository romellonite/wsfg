import json

def getAllFaq(data): 
    return data.get("faq")

def getTrendingItems(data): 
    trendingItems = []
    for item in data.get("items"):
        if(item.get("isTrending")):
            trendingItems.append(item)
    return trendingItems

def sendQuestion(questionForm): 
    return