import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Components
import { HomePageComponent } from './components/home-page/home-page.component';
import { StorePageComponent } from './components/store-page/store-page.component';
import { QAndAPageComponent } from './components/q-and-a-page/q-and-a-page.component';
import { SignInPageComponent } from './components/sign-in-page/sign-in-page.component';
import { CartPageComponent } from './components/cart-page/cart-page.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: 'store', component: StorePageComponent },
  { path: 'q-and-a', component: QAndAPageComponent },
  { path: 'sign-in', component: SignInPageComponent },
  { path: 'cart', component: CartPageComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomePageComponent]
