import { Pipe, PipeTransform } from '@angular/core';
/**
 * This pipe returns the text depending on the snack bar type
 */
@Pipe({
    name: 'snackBarText'
})
export class SnackBarTextPipe implements PipeTransform {
  transform(dataSubmitted: boolean, snackBarType: string): any {
    if(snackBarType.includes('message')){
      if(dataSubmitted){
        return {confirmationText: 'SUCCESS', remainingText: 'Question Sent'};
      }
      return {confirmationText: 'ERROR', remainingText: 'Question Not Sent'};
    }
    if(snackBarType.includes('promotionCode')){
      if(dataSubmitted){
        return {confirmationText: 'SUCCESS', remainingText: 'Promotion Code Valid'};
      }
      return {confirmationText: 'NOPE', remainingText: 'Invalid Promotion Code'};
    }
  }
}