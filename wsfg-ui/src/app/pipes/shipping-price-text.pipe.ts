import { Pipe, PipeTransform } from '@angular/core';
/**
 * This pipe returns the text for the shipping price
 */
@Pipe({
    name: 'shippingPriceText'
})
export class ShippingPriceTextPipe implements PipeTransform {
  transform(cartItems: any, shippingPrice: number): number {
    cartItems.forEach(item => {
      if(!item.shippable){
        return shippingPrice;
      }
    });
    return 0;
  }
}