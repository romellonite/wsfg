import { Pipe, PipeTransform } from '@angular/core';
// Forms
import { FormGroup } from '@angular/forms';

/**
 * This pipe provides the validation for specific form validators 
 */
@Pipe({ name: 'formInputErrors' })
export class FormInputErrorsPipe implements PipeTransform {
  // The minimum characters for the message
  transform(form: FormGroup, inputName: string, formValues: string, minimum_characters?: any): string {
    // If input is valid or has not changed just return
    if(!this.isInputChanged(form, inputName) || !this.isInputInvalid(form, inputName)){
      return;
    }
    // If the form input name is 'EMAIL' provide the correct error input messages
    if(inputName.includes('email')){
      if(this.isInputErrorsRequired(form, inputName) && this.isInputChanged(form, inputName)){
        return '* Email Is Required';
      }
      else if(this.isInputErrorsEmail(form, inputName))
      {
        return '* Email must be a valid email address';
      }
    }
    // If the form input name is 'MESSAGE' provide the correct error input messages
    if(inputName == ('message')){
      if(this.isInputChanged(form, inputName) && this.isInputInvalid(form, inputName) && minimum_characters.message){
        return '* Message must have more than ' + (minimum_characters.message-1) + ' characters';
      }
    }
  }

  /**
   * Returns the state of whether errors are required
   * @param inputName the input name
   */
  private isInputErrorsRequired(form: FormGroup, inputName: string): boolean{
    // nameField.errors && nameField.errors.required
    return form.get(inputName).errors?.required;
  }

  /**
   * Returns the state of whether the input was tocuhed
   * @param inputName the input name
   */
  private isInputChanged(form: FormGroup, inputName: string): boolean{
    return form.get(inputName).dirty;
  }

  /**
   * Returns the state of whether input errors are email
   * @param inputName the input name
   */
  private isInputErrorsEmail(form: FormGroup, inputName: string): boolean{
    return form.get(inputName).errors.email;
  }

  /**
   * Returns the state of whether the input is invalid
   * @param inputName the input name
   */
  private isInputInvalid(form: FormGroup, inputName: string): boolean{
    return form.get(inputName).invalid;
  }

}