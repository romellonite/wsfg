import { Pipe, PipeTransform } from '@angular/core';
// Forms
import { FormGroup } from '@angular/forms';

/**
 * This pipe provides the state of whether the form is ready to submit
 */
@Pipe({
    name: 'formReadyToSubmit'
})
export class FormReadyToSubmitPipe implements PipeTransform {
  transform(form: FormGroup, formValues: any): boolean {
    for (const inputName in form.controls) {
      if(form.get(inputName).invalid){
        return true;
      }
    }
    return false;
  }

}