import { Pipe, PipeTransform } from '@angular/core';
/**
 * This pipe returns the text of whether the item is in stock
 */
@Pipe({
    name: 'itemIsInStock'
})
export class ItemInStockPipe implements PipeTransform {
  transform(itemIsInStock: boolean): string {
    if(itemIsInStock){
      return 'IN STOCK!';
    }
    return 'OUT OF STOCK!';
  }
}