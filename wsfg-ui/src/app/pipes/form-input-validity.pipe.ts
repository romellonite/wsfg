import { Pipe, PipeTransform } from '@angular/core';
// Forms
import { FormGroup } from '@angular/forms';

/**
 * This pipe provides the validation for a forms input
 */
@Pipe({
    name: 'formInputValidityPipe'
})
export class FormInputValidityPipe implements PipeTransform {
  transform(form: FormGroup, inputName: string, formValues: any): boolean {
    return this.isInputChanged(form, inputName) && this.isInputInvalid(form, inputName);
  }

  /**
   * Returns the state of whether the input was tocuhed
   * @param inputName the input name
   */
  private isInputChanged(form: FormGroup, inputName: string): boolean{
    return form.get(inputName).dirty;
  }

  /**
   * Returns the state of whether the input is invalid
   * @param inputName the input name
   */
  private isInputInvalid(form: FormGroup, inputName: string): boolean{
    return form.get(inputName).invalid;
  }

}