import { Pipe, PipeTransform } from '@angular/core';
/**
 * This pipe filters a list by the given filter
 */
@Pipe({
    name: 'filterList'
})
export class FilterListPipe implements PipeTransform {
  transform(list: any, originalList: any, filter: string): any[] {
    let filteredList = [];
    if(filter == 'All'){
      return originalList;
    }
    list.forEach(item => {
      if(item.filter == filter){
        filteredList.push(item);
      }
    });
    return filteredList;
  }
}