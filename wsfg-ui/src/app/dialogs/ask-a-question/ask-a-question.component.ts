import { Component, OnInit } from '@angular/core';
// Dialog
import { MatDialogRef } from '@angular/material/dialog';
// Services
import { FormService } from '../../services/form-service.service';

@Component({
  selector: 'app-ask-a-question',
  templateUrl: './ask-a-question.component.html',
  styleUrls: ['./ask-a-question.component.scss']
})
export class AskAQuestionComponent implements OnInit {

  // Form Data
  formData: any = {};

  constructor(
    private dialogRef: MatDialogRef<AskAQuestionComponent>, 
    private formService: FormService) { }

  ngOnInit(): void {
    // Set the Form data
    this.formData = this.formService.getFormData();
  }

  /**
   * This method submits the question
   */
  submitQuestion(){
    this.dialogRef.close(this.formData.form.value); // Close Pop Up
  }

  /**
   * This method closes the pop up.
   */
  closePopUp(){
    this.dialogRef.close(); // Close Pop Up
  }

}
