import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorePageService {

  constructor() { }

  // Returns the Filter Data
  getFilterData(): any[]{
    return [
      {
        name: 'Filters',
        children: [
          {name: 'Games'},
          {name: 'Sort By'}
        ]
      }
    ];
  }

  // Returns the mock filter getFilterByGames data
  getFilterByGames(): Observable<any>{
    return of(['Destiny 2', 'All']);
  }

  // Returns the mock products
  getProducts(): Observable<any>{
    return of([
      {
        id: 0,
        name:'Not Forgotten',
        price:'$25 (On Sale)',
        image:'https://i.ytimg.com/vi/cRT8H_KitxY/maxresdefault.jpg'
      },
      {
        id: 1,
        name:'Trials Of Osiris',
        price:'$15 Per Character',
        image:'https://static3.gamerantimages.com/wordpress/wp-content/uploads/2020/03/destiny-2-trials-of-osiris-logo.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5'
      },
      {
        id: 2,
        name:'Legend Rank',
        price:'$10 Per 1900 Glory Points',
        image:'https://carrysquad.com/uploads/Product/cb9fa4be28a00ec978ebcbda6d98f62faa84c8b4@2x.jpg'
      },
      {
        id: 3,
        name:'Recluse',
        price: '$25 (On Sale)',
        image:'https://d1lss44hh2trtw.cloudfront.net/assets/article/2019/03/05/destiny-2-the-recluse-pinnacle-weapon_feature.jpg'
      }
    ]);
  }
}
