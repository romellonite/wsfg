import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartPageService {

  constructor() { }

  // Returns the carts items
  getCartItems(): Observable<any>{
    return of([
      {
        id: 0,
        itemNumber: 'AAAAAAAAAA',
        inStock: true,
        game: 'Destiny 2',
        name:'Not Forgotten',
        price: 25.00,
        shippable: false,
        maxQuantity: 1,
        image:'https://i.ytimg.com/vi/cRT8H_KitxY/maxresdefault.jpg'
      },
      {
        id: 1,
        itemNumber: 'ABAAAAAAAA',
        inStock: true,
        game: 'Destiny 2',
        name:'Trials Of Osiris',
        price: 15.00,
        shippable: false,
        maxQuantity: 3,
        image:'https://static3.gamerantimages.com/wordpress/wp-content/uploads/2020/03/destiny-2-trials-of-osiris-logo.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5'
      },
      {
        id: 2,
        name:'Legend Rank',
        inStock: true,
        game: 'Destiny 2',
        itemNumber: 'ABCAAAAAAA',
        price: 10.00,
        shippable: false,
        maxQuantity: 3,
        image:'https://carrysquad.com/uploads/Product/cb9fa4be28a00ec978ebcbda6d98f62faa84c8b4@2x.jpg'
      },
      {
        id: 3,
        name:'Recluse',
        inStock: true,
        game: 'Destiny 2',
        itemNumber: 'ABCDAAAAAA',
        price: 25.00,
        shippable: false,
        maxQuantity: 1,
        image:'https://d1lss44hh2trtw.cloudfront.net/assets/article/2019/03/05/destiny-2-the-recluse-pinnacle-weapon_feature.jpg'
      }
    ]);
  }

  getPromotionReward(promotionCode: string): Observable<any>{
    if(promotionCode != 'ABCDEFGHIJ'){
      return of(null);
    }
    return of({ 
      id: 'discount', 
      amount: .10, 
      description: '10% Discount!'
    });
  }
}
