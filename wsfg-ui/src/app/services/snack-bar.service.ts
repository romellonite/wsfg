import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../components/snack-bar/snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  // The amout of time the snack bar stays open
  DURATION: number = 4500;

  constructor(private snackBar: MatSnackBar) { }

  /**
   * This method opens the snack bar
   * @param data the data sent to the component
   */
  openSnackBar(snackBarType: string, snackBarData?: any): void{
    snackBarData.snackBarType = snackBarType;
    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: this.DURATION,
      data: snackBarData,
      panelClass: 'snackBar'
    });
  }
}