import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QAndAPageService {

  constructor() { }

  /**
   * Returns mock questions
   */
  getQuestions(): Observable<any>{
    let questions = [];
    const FILTERS = ['General', 'Staff', 'Contact', 'Payment',  'Returns', 'Careers'];
    // const THE_QUESTIONS = ["A"];
    const THE_QUESTIONS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    for(var i = 0; i < THE_QUESTIONS.length; i++){
      questions.push({
        id: i, 
        theQuestion: 'Question: ' + THE_QUESTIONS[i],
        theAnswer: 'Answer: ' + THE_QUESTIONS[i],
        filter: FILTERS[Math.floor(Math.random()*FILTERS.length)]
      });
    }
    return of(questions);
  }

  /**
   * Returns filters
   */
  getFilters(): any[]{
    return ['All', 'General', 'Staff', 'Contact', 'Payment',  'Returns', 'Careers'];
  }
}
