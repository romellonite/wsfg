import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomePageService {

  constructor() { }

  gethomePageData(): Observable<any>{
    return of ({
      //Home Data
      homeArea:{
        titleA:'You want the Loot?',
        titleB:'We want to Slay!',
        image:'https://images2.alphacoders.com/239/23920.jpg',
        blurbs:[
          {
            id:'service',
            constantInfo: '$15 Flawless Card!PRE-ORDER NOW!',
            hoverInfo: 'View $15 Flawless Card',
            alignment:'start start'
          },
          {
            id:'about',
            constantInfo: 'Been Servicing Players Since 2017',
            hoverInfo: 'More About WSFG',
            alignment:'center end'
          },
          {
            id:'players',
            constantInfo: 'Competitive Players With Up to 2.7KDs!',
            hoverInfo: 'View These Monsters',
            alignment:'start start'
          },
          {
            id:'reviews',
            constantInfo: '"Quick, Efficent and Proffessional"~ Savzeno',
            hoverInfo: 'More Reviews',
            alignment:'center end'
          }
        ]
      },
      //Trending Area
      trendingArea:{
        title:'Destiny 2 Trending !',
        image:'https://images4.alphacoders.com/866/thumb-1920-866221.jpg',
        thumbNails:[
          {
            name:'Not Forgotten',
            price:'$25 (On Sale)',
            image:'https://i.ytimg.com/vi/cRT8H_KitxY/maxresdefault.jpg'
          },
          {
            name:'Trials Of Osiris',
            price:'$15 Per Character',
            image:'https://static3.gamerantimages.com/wordpress/wp-content/uploads/2020/03/destiny-2-trials-of-osiris-logo.jpg?q=50&fit=crop&w=960&h=500&dpr=1.5'
          },
          {
            name:'Legend Rank',
            price:'$10 Per 1900 Glory Points',
            image:'https://carrysquad.com/uploads/Product/cb9fa4be28a00ec978ebcbda6d98f62faa84c8b4@2x.jpg'
          },
          {
            name:'Recluse',
            price: '$25 (On Sale)',
            image:'https://d1lss44hh2trtw.cloudfront.net/assets/article/2019/03/05/destiny-2-the-recluse-pinnacle-weapon_feature.jpg'
          }
        ],
        moreProducts:{
          title:'View More Destiny 2 Products',
          image:'https://images3.alphacoders.com/841/thumb-1920-841918.jpg'
        }
      },
      //Tutorial Area
      tutorialArea:{
        titleA:'3 Easy Steps',
        image:'https://images2.alphacoders.com/989/thumb-1920-989919.png',
        steps:[
          {
            number:'1',
            info:'Pick Your Service',
            alignment:'start start',
            extraInfo:
            {
              info:'Choose From a Variety of Different Gaming Services!',
              buttonInfo:'View Services'
            }
          },
          {
            number:'2',
            info:'Pay Through Secure Checkout',
            alignment:'start start',
            extraInfo:
            {
              info:'Currently Accepting VISA Paypal!',
              buttonInfo:''
            }
          },
          {
            number:'3',
            info:'Email Confirmation',
            alignment:'start start',
            extraInfo:
            {
              info:'Recieve Confirmation Email of Order and Details',
              buttonInfo:''
            }
          }
        ],
        titleB:'Money Back Guaranteed!'
      }});
  }
}
