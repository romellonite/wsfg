import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {

  constructor() { }

  getNavBarData(): Observable<any>{
    return of ({
      titleArea:{
        title:'We Slay For Glimmer'
      },
      navArea:{
        navs:[
          {
            link:'sign-in',
            icon:'account_circle',
            toolTip:'Sign In'
          },
          {
            link:'store',
            icon:'store',
            toolTip:'Store'
          },
          {
            link:'cart',
            icon:'shopping_cart',
            toolTip:'Cart'
          },
          {
            link:'q-and-a',
            icon:'contact_support',
            toolTip:'FAQ'
          }
        ]
      }
    });
  }
}
