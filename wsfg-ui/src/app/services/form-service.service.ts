import { Injectable } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  MINIMUM_MESSAGE_CHARACTERS = 26;

  constructor() { }
  
  /**
   * This method returns a object of Form Data structured specifically for this application.
   */
  getFormData(): any{
    return {
      form: new FormGroup({
        message: new FormControl('',[Validators.required, Validators.minLength(this.MINIMUM_MESSAGE_CHARACTERS)]),
        email: new FormControl('',[Validators.required, Validators.email]),
        password: new FormControl('',[Validators.required, Validators.email]),
      }),
      minimumCharacters: {
        message: this.MINIMUM_MESSAGE_CHARACTERS
      }
    }
  }
}
