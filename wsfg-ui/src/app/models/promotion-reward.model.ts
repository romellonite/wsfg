export class PromotionReward {
    id: string; 
    amount: number;
    description: string;
}