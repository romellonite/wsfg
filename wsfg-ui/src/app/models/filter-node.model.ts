export class FilterNode {
    name: string;
    children?: FilterNode[];
}