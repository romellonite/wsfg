import { Component, OnInit } from '@angular/core';
import { NavBarService } from 'src/app/services/nav-bar.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private navBarService: NavBarService) { }

  data: any;
  ngOnInit() {
    this.navBarService.getNavBarData().subscribe(data => {
      this.data = data;
    })
  }

}
