import { Component, OnInit } from '@angular/core';
// Services
import { CartPageService } from 'src/app/services/cart-page.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
// Models
import { SelectionModel } from '@angular/cdk/collections';
import { PromotionReward } from '../../models/promotion-reward.model';


@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnInit {

  // Holds the cart items
  cartItems: any;
  // Holds the subtotal
  subTotal: number;
  // Holds the promotion reward name
  promotionRewardDescription: string = 'None';
  // Holds the promotion reward
  promotionReward: PromotionReward;
  // Holds the estimated total
  estimatedTotal: number;
  // Holds the shipping fee
  shippingFee: number = 3.00;
  // Holds the quantity selections
  promotionSelection = new SelectionModel<any>(true, []);

  constructor(private cartPageService: CartPageService,
              private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.cartPageService.getCartItems().subscribe(cartItems => {
      this.addQuantityFieldToList(cartItems);
      this.cartItems = cartItems; // Set cart items
      this.subTotal = this.getSubTotal(); // Set subtotal
      this.estimatedTotal = this.getEstimatedTotal(); // Set estimated total
    })
  }

  /**
   * This method gets the subtotal price of the items in the cart
   * @param cartItems the items in the cart
   */
  getSubTotal(): number{
    let estimatedTotal = 0;
    this.cartItems.forEach(item => {
      estimatedTotal+=item.price * item.quantity;
    });
    return estimatedTotal;
  }

  /**
   * This method gets the estimated total
   */
  getEstimatedTotal(): number{
    let estimatedTotal = this.subTotal - this.shippingFee;
    if(!this.promotionSelection.isEmpty()){
      if(this.promotionSelection.selected[0].id == 'discount'){
        estimatedTotal = estimatedTotal - (this.promotionSelection.selected[0].amount * estimatedTotal);
      }
    }
    return estimatedTotal;
  }

  /**
   * This method adds the quantity field to the each item object in the cart.
   * @param cartItems the items in the cart
   */
  addQuantityFieldToList(cartItems: any): any{
    cartItems.forEach(item => {
      item.quantity = 1;
    });
  }

  /**
   * This method refreshes/resets the current reward
   */
  refreshReward(): void{
    this.promotionSelection.clear();
    this.promotionRewardDescription = "None";
    this.estimatedTotal = this.getEstimatedTotal();
  }

  /**
   * This method adds the promotion reward to the summary and updates the subtotal and estimated total.
   * @param promotionReward the current promotion reward
   */
  addReward(promotionReward: any): void{
    this.promotionSelection.toggle(promotionReward);
    this.promotionRewardDescription = promotionReward.description; // Set promotion reward name
    this.estimatedTotal = this.getEstimatedTotal(); // Set the estimated total with the promotion reward
  }

  /**
   * This method updates the sub/estimated total with each quantity for the cart items.
   */
  updateItemQuantity(): any{
    this.subTotal = this.getSubTotal(); // Set subtotal
    this.estimatedTotal = this.getEstimatedTotal(); // Set estimated total
  }

  /** ABCDEFGHIJ
   * This method sets the promotion reward
   * @param promotionCode the code used for the promotion
   */
  setPromotionReward(promotionCode: string): any{
    this.cartPageService.getPromotionReward(promotionCode).subscribe(promotionReward => {
      if(promotionReward){
        this.addReward(promotionReward);
        this.snackBarService.openSnackBar('promotionCode', { reward: promotionReward.description, dataSubmitted: true });
      } else {
        this.refreshReward();
        this.snackBarService.openSnackBar('promotionCode', { dataSubmitted: false });
      }
    });
  }

  /**
   * This method deletes a item from the cart
   * @param itemNumber the item number for the deleted item
   */
  delete(itemNumber: any){
    this.cartItems = this.cartItems.filter(item => item.itemNumber !== itemNumber);
    this.subTotal = this.getSubTotal(); // Set subtotal
    this.estimatedTotal = this.getEstimatedTotal();
  }

}
