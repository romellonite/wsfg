import { Component, OnInit } from '@angular/core';
import { HomePageService } from '../../services/home-page.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(private homePageService: HomePageService) { }

  data: any;
  showTutorialBtnExtraInfo = false;

  ngOnInit(): void {
    this.homePageService.gethomePageData().subscribe(data => {
      this.data = data;
    })
  }

  tutorialAreaBtnsHover(mouseState){
    if(mouseState == 'entered')
    {
      this.showTutorialBtnExtraInfo = true;
      // document.getElementById("myText").disabled = true;
    }
    else
    {
      this.showTutorialBtnExtraInfo = false;
      // document.getElementById("myText").disabled = true;
    }
  }

  homeAreaBtnsHover(button, mouseState){
    for(let blurb of this.data.homeArea.blurbs)
    {
      if(blurb.id == button.target.id)
      {
        if(mouseState == 'entered')
        {
          document.getElementById(button.target.id).childNodes[0].textContent = blurb.hoverInfo;
        }
        else
        {
          document.getElementById(button.target.id).childNodes[0].textContent = blurb.constantInfo;
        }
      }
    }
  }

}
