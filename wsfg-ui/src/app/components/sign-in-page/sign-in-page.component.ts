import { Component, OnInit } from '@angular/core';
import { FormService } from '../../services/form-service.service';

@Component({
  selector: 'app-sign-in-page',
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.scss']
})
export class SignInPageComponent implements OnInit {

  formData: any;
  constructor(private formService: FormService) { }

  ngOnInit(): void {
    // Set the Form data
    this.formData = this.formService.getFormData();
  }

  onCreate() {
    
  }

}
