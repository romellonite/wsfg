import { Component, OnInit } from '@angular/core';
// Dialog
import { MatDialog } from '@angular/material/dialog';
// Form
import { FormControl } from '@angular/forms';
// Models
import { SelectionModel } from '@angular/cdk/collections';
// Services
import { QAndAPageService } from '../../services/q-and-a-page.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
// Components
import { AskAQuestionComponent } from '../../dialogs/ask-a-question/ask-a-question.component';


@Component({
  selector: 'app-q-and-a-page',
  templateUrl: './q-and-a-page.component.html',
  styleUrls: ['./q-and-a-page.component.scss']
})
export class QAndAPageComponent implements OnInit {

  // Hold the original questions
  ogQuestions = [];
  // Holds the questions
  questions = [];
  // Holds the filters
  filters = [];
  // Holds the answer
  answer: string;
  // Holds the question selections
  questionSelection = new SelectionModel<any>(true, []);
  // Holds the filter selections
  filterSelection = new SelectionModel<any>(true, []);
  // Tool tip position
  chatBubbleToolTipPosition = new FormControl('above');
  // Holds the current selected question class
  questionSelectedClass = 'questionSelectedScrollingDown';

  constructor(
      private questionsService: QAndAPageService, 
      private snackBarService: SnackBarService, 
      private dialog: MatDialog) { }

  ngOnInit(): void {
    // Calls the get questions service
    this.questionsService.getQuestions().subscribe(questions => {
      // Set questions
      this.questions = questions;
      // Set original questions
      this.ogQuestions = questions;
    });
    // Set Filters
    this.filters = this.questionsService.getFilters();
    // Set the answer to first question answer
    this.answer = this.questions[0].theAnswer;
    // Toggle on the first button
    this.questionSelection.toggle('btn_' + this.questions[0].id);
    // Toggle on the first filter
    this.filterSelection.toggle(this.filters[0]);
  }

  /**
   * This method selects a question
   * @param questionId the question id
   */
  selectQuestion(questionId: any){
    // Deselect the current selected question
    this.questionSelection.deselect(this.questionSelection.selected[0]);
    // Toggle on the selected question
    this.questionSelection.toggle('btn_' + questionId);
    // Set the current answer to the selected question's answer
    this.answer = this.questions.find(question =>{if(question.id == questionId){return question;}}).theAnswer;
  }
  
  /**
   * This method pins the selected button to either the top or bottom of the scrolling area depending on 
   * what direction the user is scrolling.
   */
  pinSelectedItem() {
    // Set the top scrolling area dimension
    let scrollingAreaTop = document.getElementById('questionArea').getBoundingClientRect().top;
    // Set the bottom scrolling area dimension
    let scrollingAreaBottom = document.getElementById('questionArea').getBoundingClientRect().bottom;
    // Set the selected item dimensions
    let selectedItem = document.getElementById(this.questionSelection.selected[0]).getBoundingClientRect();
    if (scrollingAreaTop >= selectedItem.top){
      // Set the question selected class for scrolling DOWN
      this.questionSelectedClass = 'questionSelectedScrollingDown';
    }
	  if (scrollingAreaBottom <= selectedItem.bottom){
      // Set the question selected class for scrolling UP
      this.questionSelectedClass = 'questionSelectedScrollingUp';
    }
  }

  /**
   * This method selects a filter 
   * @param filter the filter
   */
  selectFilter(filter: string){
    // Set questions to filtered questions
    this.questions = this.filterQuestions(filter);
    // Deselects the current selected filter
    this.filterSelection.deselect(this.filterSelection.selected[0]);
    // Toggle on the selected filter
    this.filterSelection.toggle(filter);
    // Select the first question
    this.selectQuestion(this.questions[0].id);
    // Scroll to the top of the questions area
    document.getElementById('questionArea').scrollTo(0, 0); 
  }

  /**
   * This method returns a list of filtered questions.
   * @param filter the current filter selected
   */
  filterQuestions(filter: string): any[]{
    // If the filter is 'All' return the ORIGINAL questions
    if(filter.includes('All')){
      return this.ogQuestions;
    }
    // Return the FILTERED questions
    return this.ogQuestions.filter(question => question.filter == filter);
  }

  /**
   * This method opens up the ask a question pop up.
   */
  openAskAQuestionModal(){
    this.dialog.open(AskAQuestionComponent, {
      height: '55vh',
      width: '55vw',
      panelClass: 'askAQuestionDialog'
    }).afterClosed().subscribe((form: any) => {
      if(form){
        this.snackBarService.openSnackBar('message', { email: form.email, dataSubmitted: true });
      }
    });
  }
}
