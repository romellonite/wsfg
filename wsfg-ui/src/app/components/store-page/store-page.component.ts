// Angular 
import { Component, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';
// Models
import { FilterNode } from '../../models/filter-node.model'
// Services
import { StorePageService } from '../../services/store-page.service';

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: ['./store-page.component.scss']
})

export class StorePageComponent implements OnInit {

  // Holds the filter tree control
  treeControl = new NestedTreeControl<FilterNode>(node => node.children);
  // Holds the filters data
  filterData = new MatTreeNestedDataSource<FilterNode>();
  // Holds the filter sort by options
  filterSortByOptions = ['Price Low To High', 'Price High To Low'];
  // Holds the filter by games options
  filterByGamesOptions = ['Destiny 2', 'All'];
  // Holds the products 
  products: any[];

  constructor(
    private storePageService: StorePageService
  ) { }

  // Checks the state of whether their is a child node present
  hasChild = (_: number, node: FilterNode) => !!node.children && node.children.length > 0;

  ngOnInit(): void {
    // Sets the filter data
    this.filterData.data = this.storePageService.getFilterData();
    // Sets the filter by games data
    this.storePageService.getFilterByGames().subscribe(filterByGamesOptions => {
      this.filterByGamesOptions = filterByGamesOptions;
    });
    // Sets the products data
    this.storePageService.getProducts().subscribe(products => {
      this.products = products;
    });
  }
}