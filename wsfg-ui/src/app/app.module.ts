import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app-material.module';
import { AppRoutingModule, routingComponents } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { StorePageComponent } from './components/store-page/store-page.component';
import { QAndAPageComponent } from './components/q-and-a-page/q-and-a-page.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { AskAQuestionComponent } from './dialogs/ask-a-question/ask-a-question.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { SignInPageComponent } from './components/sign-in-page/sign-in-page.component';
import { CartPageComponent } from './components/cart-page/cart-page.component';

// Pipes
import { FilterListPipe } from './pipes/filter-list.pipe';
import { FormInputErrorsPipe } from './pipes/form-input-errors.pipe';
import { FormInputValidityPipe } from './pipes/form-input-validity.pipe';
import { FormReadyToSubmitPipe } from './pipes/form-ready-to-submit.pipe';
import { SnackBarTextPipe } from './pipes/snack-bar-text.pipe';
import { ItemInStockPipe } from './pipes/item-in-stock-text.pipe';
import { ShippingPriceTextPipe } from './pipes/shipping-price-text.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    routingComponents,
    StorePageComponent,
    QAndAPageComponent,
    FilterListPipe,
    AskAQuestionComponent,
    FormInputErrorsPipe,
    FormInputValidityPipe,
    FormReadyToSubmitPipe,
    SnackBarComponent,
    SnackBarTextPipe,
    SignInPageComponent,
    CartPageComponent,
    ItemInStockPipe,
    ShippingPriceTextPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
